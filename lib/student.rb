require 'byebug'

class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    @course_load = Hash.new(0)
  end

  def name
    @first_name + ' ' + @last_name
  end

  def enroll(new_course)
    raise('wtf?') if enroll_conflict? new_course
    @courses << new_course unless @courses.include? new_course
    new_course.students << self unless new_course.students.include? self
  end

  def course_load
    # byebug
    course_load_hash = Hash.new(0)
    @courses.each do |course|
      course_load_hash[course.department] += course.credits
    end
    course_load_hash
  end

  def enroll_conflict?(new_course)
    @courses.any? { |course| course.conflicts_with? new_course }
  end

end
